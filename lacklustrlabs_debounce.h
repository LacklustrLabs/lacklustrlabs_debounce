#pragma once

#if defined(ARDUINO) && ARDUINO >=100
#include <Arduino.h>
#else
#include <WProgram.h>
#endif
/*
  This Debouncer is based on code found at http://www.arduino.cc/en/Tutorial/Debounce

  created 21 November 2006
  by David A. Mellis
  modified 30 Aug 2011
  by Limor Fried
  modified 28 Dec 2012
  by Mike Walters

  This example code is in the public domain.

*/

#ifndef LACKLUSTRLABS_DEBUG_PORT
#define LACKLUSTRLABS_DEBUG_PORT Serial
#endif // LACKLUSTRLABS_DEBUG_PORT

namespace lacklustrlabs {

class Debounce {
  public:
    Debounce(uint8_t pin, uint32_t debounceDelay=100, uint32_t holdAndRepeatDelay=0, bool repeatOnHigh=true ):
      _pin(pin),
      _debounceDelay(debounceDelay),
      _holdAndRepeatDelay(holdAndRepeatDelay),
      _lastDebounceTime(millis()),
      _repeatEnabled(holdAndRepeatDelay>0),
      _repeatOnHigh(repeatOnHigh){

      begin();
    }

    Debounce( const Debounce& other ) = delete; // non construction-copyable
    Debounce& operator=( const Debounce& ) = delete; // non copyable
    Debounce() = delete;

    /**
       begin() does not set pinMode()
     */
    void begin() {
      _lastDebounceTime = millis();
      _lastReading = _lastOutputState = digitalRead(_pin);
    }

    /**
       return true if the state changed.
       @param state the new value of the state
      */
    bool getState(bool& state) {
      static bool repeat = false;
      return getState(state, repeat);
    }

    /**
       return true if the state changed.
       @param state the new value of the state
    */
    bool getState(bool& state, bool& repeat) {
      // read the state of the switch into a local variable:
      bool reading = digitalRead(_pin);
      uint32_t now = millis();
      repeat=false;

      // check to see if you just pressed the button
      // (i.e. the input went from LOW to HIGH),  and you've waited
      // long enough since the last press to ignore any noise:

      // If the switch changed, due to noise or pressing:
      if (reading != _lastReading) {
        // reset the debouncing timer
        _lastDebounceTime = now;
      }
      if ((now - _lastDebounceTime) > _debounceDelay) {
#ifdef LACKLUSTRLABS_DEBOUNCE_SERIAL_DEBUG
        LACKLUSTRLABS_DEBUG_PORT.print("Debounce::getState time:");
        LACKLUSTRLABS_DEBUG_PORT.print(now - _lastDebounceTime);
        LACKLUSTRLABS_DEBUG_PORT.print(" reading:");
        LACKLUSTRLABS_DEBUG_PORT.print(reading ? "true" : "false");
        LACKLUSTRLABS_DEBUG_PORT.print(" _lastReading:");
        LACKLUSTRLABS_DEBUG_PORT.print(_lastReading ? "true" : "false");
        LACKLUSTRLABS_DEBUG_PORT.print(" _lastOutputState:");
        LACKLUSTRLABS_DEBUG_PORT.println(_lastOutputState ? "true" : "false");
#endif // LACKLUSTRLABS_DEBOUNCE_SERIAL_DEBUG
        // whatever the reading is at, it's been there for longer
        // than the debounce delay, so take it as the actual current state:

        // if the button state has changed:
        if (reading != _lastOutputState) {

#ifdef LACKLUSTRLABS_DEBOUNCE_SERIAL_DEBUG
          LACKLUSTRLABS_DEBUG_PORT.print("Debounce::getState toggled state to: ");
          LACKLUSTRLABS_DEBUG_PORT.println(reading ? "true" : "false");
#endif // LACKLUSTRLABS_DEBOUNCE_SERIAL_DEBUG
          state = _lastReading = _lastOutputState = reading;
          return true;
        } else if ( _repeatEnabled && reading == _repeatOnHigh && _holdAndRepeatDelay > _debounceDelay && ((now - _lastDebounceTime) > _holdAndRepeatDelay) ) {

          _lastDebounceTime += _holdAndRepeatDelay - _debounceDelay;
          state = reading;
          repeat = true;
          return true;
        }
      }
      _lastReading = reading;
      state = _lastOutputState;
      return false;
    }

  protected:
    const uint8_t _pin;
    uint32_t _debounceDelay;
    uint32_t _holdAndRepeatDelay;
    uint32_t _lastDebounceTime;
    bool _repeatEnabled;
    bool _lastOutputState;
    bool _lastReading;
    bool _repeatOnHigh;

};

} // namespace lacklustrlabs

