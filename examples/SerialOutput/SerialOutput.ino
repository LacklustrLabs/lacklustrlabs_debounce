#include <lacklustrlabs_debounce.h>

#if defined(ARDUINO_ARCH_STM32F1)
#define SOUT Serial1
constexpr uint8_t BUTTON_PIN = 32;
#else
#define SOUT Serial
constexpr uint8_t BUTTON_PIN = 4;
#endif

using namespace lacklustrlabs;

// debouncer will repeat if button==false and if pressed more than 500ms.
Debounce dbPin(BUTTON_PIN, 50, 500);

void setup() {
  pinMode(BUTTON_PIN, INPUT);
  SOUT.begin(115200);
  SOUT.print(F("Starting Debouncer on pin "));
  SOUT.println(F("Hold button down more than 0.5 seconds for repeat action."));
  SOUT.print(F("BUTTON_PIN="));
  SOUT.println(BUTTON_PIN);
  dbPin.begin();
}

void loop() { 
  bool buttonState;
  bool repeat;
  if (dbPin.getState(buttonState, repeat)) {
    if (repeat) {
      SOUT.print(F("Repeated button is ")); 
    } else {
      SOUT.print(F("Button is "));
    }
    SOUT.println(buttonState?F("high"):F("low"));
  }
}